## ![Hero2](https://gitlab.com/yohanyanev/QT_Game/-/raw/master/Space%20Heroes/resources/heros/2hero_Down.png) ![Hero1](https://gitlab.com/yohanyanev/QT_Game/-/raw/master/Space%20Heroes/resources/heros/1hero_Down.png) ![Hero3](https://gitlab.com/yohanyanev/QT_Game/-/raw/master/Space%20Heroes/resources/heros/3hero_Down.png) Space Heroes
 [![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
[![Windows](https://svgshare.com/i/ZhY.svg)](https://svgshare.com/i/ZhY.svg)
[![Linux](https://svgshare.com/i/Zhy.svg)](https://svgshare.com/i/Zhy.svg)
[![macOS](https://svgshare.com/i/ZjP.svg)](https://svgshare.com/i/ZjP.svg)

![Maintainer](https://img.shields.io/badge/maintainer-yohanyanev-blue)
![QT](https://img.shields.io/badge/Qt->=v6.0-orange)

## Introduction    

Space Heroes is a basic 2D game.<br>
The goal is to achieve as many points as possible by eliminating the enemies.<br>
It **does not** support multiplayer but in the future I will surely desire to add this feature.
## Installation

Use the Makefile to install it
```bash
 make install
 ./game
```


## How to play
Use
<kbd>↑</kbd>
<kbd>↓</kbd>
<kbd>→</kbd>
<kbd>←</kbd>
to move around.\
Shoot projectiles  with <kbd>Space</kbd>

## License

[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
[![License: GPL v2](https://img.shields.io/badge/License-GPL_v2-blue.svg)](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html)

This is an open source project. It's free to download and tweak.

## Authors

- [@yohanyanev](https://www.gitlab.com/yohanyanev)

