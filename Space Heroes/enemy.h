#ifndef ENEMY_H
#define ENEMY_H
#include <QGraphicsRectItem>
#include <QGraphicsItem>
#include <QObject>
#include <stdlib.h>
#include <QTimer>
#include <time.h> 
#include <QBrush> 
#include <QList>


class Enemy : public QObject, public QGraphicsRectItem{
    Q_OBJECT
public:
    Enemy();
    
public slots: 
    void move();
};

#endif //ENEMY_H

//slots = member functions connected to sigals //+ need  qobject