#include "lifes.h"
#include "window.h"

extern MainWindow * mainWindow;

Lifes::Lifes()
{
    lifes = 5; 
    setPlainText( QString::number(lifes));
    setDefaultTextColor(Qt::red);
    setFont(QFont("Helvetica [Cronyx]", 22,QFont::Bold));
}


void Lifes::die()
{
    if(lifes > 0)
    {
        lifes -=1;
        setPlainText( QString::number(lifes));
    }
    
    if(lifes  <= 0 )
    {
        setPlainText( QString("GAME OVER"));
        setPos(400,230);
    }
       
}

void Lifes::reset()
{
    setPos(870,0);
    lifes = 5; 
    setPlainText( QString::number(lifes));
    mainWindow-> score-> reset();
}

int Lifes::getLifes()
{
    return lifes;
}
