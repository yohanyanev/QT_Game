#include "window.h"
#include "rect.cpp"
#include "enemy.h"
#include "score.cpp"
#include "lifes.cpp"
#define LEVEL1 "resources/backgr/bg1.gif"
#define LEVEL2 "resources/backgr/bg2.gif"

MainWindow::MainWindow(std::string hero) :
QWidget()
{
  createMenu();
  setWindowTitle(tr("Space Heros"));
  myHero = hero;
  //Widgets
  QVBoxLayout * mainLayout = new QVBoxLayout;
  mainLayout->addWidget(menuBar);

  // Add background
  QGraphicsScene * scene =  new QGraphicsScene();
  QLabel *gif_anim = new QLabel();
  QMovie *movie = new QMovie(LEVEL1);
  gif_anim->setMovie(movie);
  movie->start();
  scene->addWidget(gif_anim);
  
  //Create score
  score = new Score();
  scene -> addItem(score);

  //Add lifes
  lifes =  new Lifes();
  lifes->setPos(870,0);
  scene -> addItem(lifes);

  //Add player
  rec = new CustomRect(hero); 
  rec-> setRect(0,0,50,50);//50x50px
  scene-> addItem(rec);
  //rectangle  will recieve keyboard events
  rec -> setFlag(QGraphicsItem::ItemIsFocusable);
  rec -> setFocus();

  //Add enemies
  timer = new QTimer();
  connect( timer,SIGNAL(timeout()),rec,SLOT(spawn()) );
  timer -> start(1500); // 1.5 seconds

  //Creates the view remove scrollbars
  QGraphicsView * view = new QGraphicsView(); 
  view->setScene(scene);
  view -> setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
  view -> setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOff ) ;
  view -> setFixedSize(900, 500);

  rec->setPos(view->width()/2,view->height() - rec->rect().height());
  mainLayout->addWidget(view); 
  setLayout(mainLayout);     
 }


void MainWindow::createMenu()
{
  menuBar  = new QMenuBar();
  fileMenu = new QMenu(tr("Game"), this);
  helpMenu = new QMenu(tr("Help"), this);
    
  //map actions to menu options
  newGame     = fileMenu->addAction(tr("New Game"));
  exitAction  = fileMenu->addAction(tr("Exit"));
  aboutAction = helpMenu->addAction(tr("About")); 
  menuBar -> addMenu(fileMenu);
  menuBar -> addMenu(helpMenu);

  connect(newGame,SIGNAL(triggered()), this, SLOT(handle_newGame()));
  connect(exitAction, SIGNAL(triggered()), this, SLOT(handleExit()));
  connect(aboutAction,SIGNAL(triggered()), this, SLOT(about()));
}

// ************ SLOT FUNCTIONS **************** //

void MainWindow::handleExit()
{
  QMessageBox::StandardButton reply;
  reply = QMessageBox::question(this, "Space Heros", "Quit?",
                                QMessageBox::Yes|QMessageBox::No);
  if (reply == QMessageBox::Yes) 
  {
    QApplication::quit();
  } 
}

void MainWindow::handle_newGame()
{
  delete timer;
  lifes-> reset();
  timer = new QTimer();
  connect( timer,SIGNAL(timeout()),rec,SLOT(spawn()) );
  timer -> start(1500); // 1.5 seconds
}

void MainWindow::about()
{
   QMessageBox::about(this, tr("About Application"),
            tr("<b>Space Heros</b> is under developpement."));
}
