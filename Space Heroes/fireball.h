#ifndef FIREBALL_H
#define FIREBALL_H

#include <QGraphicsRectItem>
#include <QGraphicsScene>
#include <typeinfo>
#include <QObject>
#include <QTimer>
#include <QMovie>
#include <QLabel>
#include <QList>


class Fireball : public QObject, public QGraphicsRectItem{
    Q_OBJECT
public:
    Fireball();

public slots: 
    void move();
};

#endif //FIREBALL_H

//slots = member functions connected to sigals //+ need  qobject
//QObject needs to be 1st