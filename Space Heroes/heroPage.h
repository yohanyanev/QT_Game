#ifndef HEROPAGE_H
#define HEROPAGE_H

#include <iostream>
#include <QWidget>
#include <QString>
#include <QMainWindow>
#include <QFileDialog>
#include <QPushButton>
#include <QMessageBox>
#include <QMovie>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QApplication>
#include "score.h" 
#include "window.h"


class HeroPage : public QWidget
{
 Q_OBJECT
 public:
  explicit HeroPage(QWidget *parent = 0);
  virtual ~HeroPage() {};
  std::string hero ;

 public slots:
  void on_startButton_clicked();
  void on_heroButton2_clicked();
  void on_heroButton1_clicked();

 private:
    QPushButton * startButton;
    QPushButton * heroButton1;
    QPushButton * heroButton2;
   
};

#endif  //HEROPAGE_H