#ifndef STARTPAGE_H
#define STARTPAGE_H

#include <iostream>
#include <QWidget>
#include <QString>
#include <QMainWindow>
#include <QFileDialog>
#include <QPushButton>
#include <QMessageBox>
#include <QMovie>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QApplication>
#include "score.h" 
#include "window.h"


class Start : public QWidget
{
 Q_OBJECT
 public:
  explicit Start(QWidget *parent = 0);
  virtual ~Start() {};
  Score * score;
  std::string hero ;

 public slots:
  void on_heroButton1_clicked();
  void on_heroButton2_clicked();
  void on_heroButton3_clicked();
  void on_startButton_clicked();

 private:
    QPushButton * startButton;
    QPushButton * heroButton1;
    QPushButton * heroButton2;
    QPushButton * heroButton3;
   
};

#endif  //STARTPAGE_H