#include "enemy.h"
#include "window.h"

extern MainWindow * mainWindow ;
Enemy::Enemy()
{
    srand(time(NULL));
    int RANDOM = rand() % 720;
    setPos(RANDOM , 0);
    setRect(0,0,50,50);
    QTimer * timer = new QTimer();
    timer -> start(50);
    connect(timer, SIGNAL(timeout()), this, SLOT(move()));
    QBrush q;
    srand(time(NULL));
    int randEnemy = rand() % 3;
    if(randEnemy == 1)
        q.setTextureImage(QImage("resources/enemies/enemy1.png"));
    else if(randEnemy == 2)
        q.setTextureImage(QImage("resources/enemies/enemy2.png"));
    else
        q.setTextureImage(QImage("resources/enemies/enemy3.png"));
    this->setBrush(q);
    this->setPen(Qt::NoPen);
}


void Enemy::move()
{
    //enemy reach end view
    setPos(x()  , y() + 5 );
    if(pos().y() + rect().height() > 520)
    {
        mainWindow -> lifes -> die();
        scene() -> removeItem(this);
        delete this;
    }
    //collision with player 
    QList<QGraphicsItem*> colItems = collidingItems();
    foreach(QGraphicsItem* item, colItems)
    {
        if (typeid(* item) == typeid(CustomRect))
        {
            mainWindow -> lifes -> die();
            scene() -> removeItem(this);
            delete this;
        } 
    }
}

