#include "rect.h"
#include "fireball.cpp"
#include "window.h"
#include "enemy.cpp"
#include "commonDef.h"

extern MainWindow * mainWindow ; 

CustomRect::CustomRect(std::string hero)
{
    heroChoice=hero;
    QBrush q;
    if(heroChoice == "1")
        q.setTextureImage(QImage(Hero1up));
    else if(heroChoice == "2")
        q.setTextureImage(QImage(Hero2up));
    else if(heroChoice == "3")
        q.setTextureImage(QImage(Hero3up));
        
    this->setBrush(q);
    this->setPen(Qt::NoPen);
}

void CustomRect::keyPressEvent(QKeyEvent * event)
{
    
    if(event->key() == Qt::Key_Left)
    {
        QBrush q;
        if(heroChoice == "1")
            q.setTextureImage(QImage(Hero1left));
        else if(heroChoice == "2")
            q.setTextureImage(QImage(Hero2left));
        else if(heroChoice == "3")
            q.setTextureImage(QImage(Hero3left));
        this->setBrush(q);
        this->setPen(Qt::NoPen);

        if(pos().x() > 0)
            setPos(x() - 15 , y());
    }
    else if(event->key() == Qt::Key_Right)
    {
        QBrush q;
        if(heroChoice == "1")
            q.setTextureImage(QImage(Hero1right));
        else if(heroChoice == "2")
            q.setTextureImage(QImage(Hero2right));
        else if(heroChoice == "3")
            q.setTextureImage(QImage(Hero3right));
        this->setBrush(q);
        this->setPen(Qt::NoPen);
        if(pos().x() + 50 < 880)
            setPos(x() + 15 , y());
    }
    else if(event->key() == Qt::Key_Up)
    {
        QBrush q;
        if(heroChoice == "1")
            q.setTextureImage(QImage(Hero1up));
        else if(heroChoice == "2")
            q.setTextureImage(QImage(Hero2up));
        else if(heroChoice == "3")
            q.setTextureImage(QImage(Hero3up));
        this->setBrush(q);
        this->setPen(Qt::NoPen);
        if(pos().y()  > 0)
            setPos(x()  , y() - 15 );
    }
    else if(event->key() == Qt::Key_Down)
    {
        QBrush q;
        if(heroChoice == "1")
            q.setTextureImage(QImage(Hero1down));
        else if(heroChoice == "2")
            q.setTextureImage(QImage(Hero2down));
        else if(heroChoice == "3")
            q.setTextureImage(QImage(Hero3down));
        this->setBrush(q);
        this->setPen(Qt::NoPen);
        if( pos().y() + 50 < 490 )
            setPos(x()  , y() + 15 );
    }
    else if(event->key() == Qt::Key_Space)
    {
       //shoot fireball
        Fireball  * fireball =  new Fireball();
        fireball -> setPos(x(),y());
        scene() ->addItem(fireball);
    }
    
}

void CustomRect::spawn()
{

  if(mainWindow->lifes -> getLifes() == 0)
  {
    mainWindow->timer -> stop();
  }
  else
  {
    Enemy * enemy = new Enemy();
    scene() -> addItem(enemy);
  } 
}

