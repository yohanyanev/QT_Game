#include <iostream>
#include <QApplication>
#include <QTextStream>
#include <QWidget>
#include <QFile>
#include "window.cpp"
#include "rect.h"
#include "fireball.h"
#include "enemy.h"
#include "startPage.h"

MainWindow * mainWindow;

int main(int argc, char *argv[]) 
{
	QApplication app(argc, argv);
	Start s;
	QFile file("stylesheet.qss");
	if (file.open(QIODevice::ReadOnly | QIODevice::Text)) 
	{
    	QTextStream stream(&file);
     	s.setStyleSheet(stream.readAll());
     	file.close();
	}
	s.show();
	return app.exec();
}
