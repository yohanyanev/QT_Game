#include "fireball.h"
#include "enemy.h"
#include "rect.h"
#include "window.h"

extern MainWindow * mainWindow ;

Fireball::Fireball(){

    QBrush q;
    if(mainWindow->myHero == "1")
        q.setTextureImage(QImage("resources/heros/1bullet.png"));
    else if(mainWindow->myHero == "2")
        q.setTextureImage(QImage("resources/heros/2bullet.png"));
    else
        q.setTextureImage(QImage("resources/heros/3bullet.png"));

    this->setBrush(q);
    this->setPen(Qt::NoPen);

    setRect(0,0,20,20);
    QTimer * timer = new QTimer();
    timer -> start(40);
    connect(timer, SIGNAL(timeout()), this, SLOT(move()));
}

void Fireball::move()
{
    QList<QGraphicsItem*> colItems = collidingItems();
    foreach(QGraphicsItem* item, colItems)
    {
        if (typeid(* item) == typeid(Enemy))
        {
            mainWindow->score->setScore();
            scene() -> removeItem(item);
            scene() -> removeItem(this);
            delete this;
            delete item;
            return;
        } 
    }
    //No collision move forward
    setPos(x()  , y() - 5 );
    if(pos().y() < 10)
    {
        scene() -> removeItem(this);
        delete this;
    }  
}

