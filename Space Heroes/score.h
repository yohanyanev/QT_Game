#ifndef SCORE_H
#define SCORE_H
#include <QGraphicsTextItem>
#include <QString>
#include <QColor>
#include <QFont>

class Score : public QGraphicsTextItem{
public:
    Score(); 
    ~Score() {};
    void setScore();
    int getScore();
    void reset();
    int points;
};

#endif //SCORE_H