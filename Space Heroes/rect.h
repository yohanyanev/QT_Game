#ifndef RECT_H
#define RECT_H
#include <QGraphicsRectItem>
#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QKeyEvent>
#include <QObject>
#include <QBrush>
#include <QList>
#include <QTime>
#include "fireball.h"

class CustomRect: public QObject,  public QGraphicsRectItem{
    Q_OBJECT
public:
    CustomRect(std::string hero);
    void keyPressEvent(QKeyEvent * event);
    std::string heroChoice;
    
public slots:
    void spawn();
};  

#endif // RECT_H