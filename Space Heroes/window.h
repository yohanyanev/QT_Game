#ifndef WINDOW_H
#define WINDOW_H
#include <QGraphicsTextItem>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QApplication>
#include <QHBoxLayout>
#include <QMainWindow>
#include <QPushButton>
#include <QMessageBox>
#include <QListView>
#include <QPalette>
#include <QMenuBar>
#include <iostream>
#include <QAction>
#include <QWidget>
#include <QString>
#include <QColor>
#include <QTimer>
#include <QMovie>
#include <QImage>
#include <QLabel>
#include <QIcon>
#include <QMenu>
#include <QFont>
#include "score.h"
#include "rect.h"
#include "lifes.h"


namespace Ui
{
   class MainWindow;
}

class MainWindow : public QWidget 
{
   Q_OBJECT
   public: 
	explicit MainWindow(std::string hero);
	virtual ~MainWindow() {};
	Score  * score;
	Lifes  * lifes;
	QTimer * timer;
	std::string myHero;
   //различните функции слотове обвързани с бутон при натискане

   private slots:
   	void handle_newGame();
	void handleExit();
	void about();
 
   private: 
	void createHorizontalGroupBox();
	void buttonBottom();
	void createMenu();

	CustomRect * rec;
	QPixmap    * pixmap;
	QIcon 	   * icon;
	QFont 	   * font;
    QMenuBar   * menuBar;  //menu bar
	QMenu      * fileMenu; //file menu opt 
	QMenu      * helpMenu; //edit menu opt
	QAction    * newGame;
    QAction    * exitAction; //action exit
	QAction    * aboutAction;

};

#endif //WINDOW_H	
