#include "startPage.h"
#include <QPushButton>
#include "startPage.h"
#include <QVBoxLayout>
#include <QIcon>
#include <QSize>
#include <QComboBox>
#include <QStringList>
#include <QGraphicsDropShadowEffect>

extern MainWindow * mainWindow;

Start::Start(QWidget *parent) :
 QWidget(parent)
 {
    setWindowTitle(tr("Space Heros"));
    QVBoxLayout * mainLayout = new QVBoxLayout;
    hero = "1";

    //add shadow to Qlabel;
    QGraphicsDropShadowEffect* effect = new QGraphicsDropShadowEffect();
    effect->setBlurRadius(1); //Adjust accordingly
    effect->setOffset(7,7); //Adjust accordingly


    setFixedSize(924, 524);
    // Add background
    QGraphicsScene * scene =  new QGraphicsScene();
    QLabel *gif_anim = new QLabel();
    QMovie *movie = new QMovie("resources/backgr/startup.gif");
    gif_anim->setMovie(movie);
    movie->start();
    scene->addWidget(gif_anim);

    QGraphicsView * view = new QGraphicsView(); 
    view->setScene(scene);
    mainLayout->addWidget(view); 
    view -> setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
    view -> setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOff ) ;
    setLayout(mainLayout);     

    heroButton1 = new QPushButton(this);
    heroButton1->setGeometry(372, 392, 120, 120);
    heroButton1->setGraphicsEffect(effect);
    //heroButton1->setStyleSheet(" background-color: Transparent; padding-left:7px; padding-top:5px;  border-width: 1px; border-radius: 10px;  font: bold 20px;");
    //heroButton1->setStyleSheet(QString("QPushButton {background-color: transparent;border-radius: 60px;} QPushButton:pressed {background-color: #fa7787;}"));
    heroButton1->setStyleSheet(QString("QPushButton {background-color: transparent;border-radius: 30px;} QPushButton:pressed {background-color:qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 transparent, stop: 0.5 transparent,stop: 0.51 transparent,stop: 0.95 #fa7787, stop: 1.0 #fa7787);}"));

    heroButton2 = new QPushButton(this);
    heroButton2->setGeometry(53, 325, 90, 80);
    heroButton2->setGraphicsEffect(effect);
    //heroButton2->setStyleSheet(" background-color: Transparent; padding-left:7px; padding-top:5px;  border-width: 1px; border-radius: 10px;  font: bold 20px;");
    //heroButton2->setStyleSheet(QString("QPushButton {background-color: transparent;border-radius: 30px;} QPushButton:pressed {background-color: #fa7787;}"));
    heroButton2->setStyleSheet(QString("QPushButton {background-color: transparent;border-radius: 30px;} QPushButton:pressed {background-color:qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 transparent, stop: 0.5 transparent,stop: 0.51 transparent,stop: 0.95 #e7ffaa, stop: 1.0 #e7ffaa);}"));
   
    heroButton3 = new QPushButton(this);
    heroButton3->setGeometry(780, 313, 100, 100);
    heroButton3->setGraphicsEffect(effect);
    heroButton3->setCheckable(true);
    heroButton3->setStyleSheet(QString("QPushButton {background-color: transparent;border-radius: 30px;} QPushButton:pressed {background-color:qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 transparent, stop: 0.5 transparent,stop: 0.51 transparent, stop: 0.71 transparent,stop: 1.0 #65446d);}"));
    //heroButton3->setStyleSheet(" background-color: Transparent;  border-width: 1px; border-radius: 30px;  font: bold 20px;");
    

    startButton = new QPushButton(this);
    startButton->setGeometry(370, 210, 140, 90);
    startButton->setGraphicsEffect(effect);
    startButton->setIcon(QIcon("resources/logo.png"));
    startButton->setIconSize(QSize(65,65));
    //startButton->setStyleSheet("background: #fa7787; padding-left:7px; padding-top:5px;  border-width: 1px; border-radius: 10px;  font: bold 20px;");
    startButton->setStyleSheet(QString("QPushButton {background: #fa7787; padding-left:7px; padding-top:5px;  border-width: 1px; border-radius: 10px;  font: bold 20px;} QPushButton:pressed {background-color: #65446d;}"));
    connect(startButton, SIGNAL (released()),this, SLOT (on_startButton_clicked()));
    connect(heroButton1, SIGNAL (released()),this, SLOT (on_heroButton1_clicked()));
    connect(heroButton2, SIGNAL (released()),this, SLOT (on_heroButton2_clicked()));
    connect(heroButton3, SIGNAL (released()),this, SLOT (on_heroButton3_clicked()));

}

void Start::on_heroButton3_clicked()
{
    hero = "3";
}

void Start::on_heroButton2_clicked()
{
    hero = "2";
}

void Start::on_heroButton1_clicked()
{
    hero = "1";
}


void Start::on_startButton_clicked()
{
    hide();
    mainWindow = new MainWindow(hero);
    mainWindow->show();
}
