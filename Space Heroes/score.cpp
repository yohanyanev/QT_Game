#include "score.h"
#define LEVE2

Score::Score()
{
    points = 0; 
    setPlainText( QString::number(points));
    setDefaultTextColor(Qt::white);
    setFont(QFont("Helvetica [Cronyx]", 22,QFont::Bold));
}


void Score::setScore()
{
    points +=1;
    setPlainText( QString::number(points));
    
}


int Score::getScore()
{
    return points;
}

void Score::reset()
{
    points = 0;
}