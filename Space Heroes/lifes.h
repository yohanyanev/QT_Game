#ifndef LIFES_H
#define LIFES_H
#include <QGraphicsTextItem>
#include <QString>
#include <QColor>
#include <QFont>

class Lifes : public QGraphicsTextItem{
public:
    Lifes(); 
    ~Lifes() {};
    void die();
    void addLife();
    int getLifes();
    void reset();
    void gameOver();
private:
    int lifes;
};

#endif //LIFES_H