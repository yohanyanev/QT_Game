#ifndef COMMONDEF_H
#define COMMONDEF_H

#define Hero1up       "resources/heros/1hero_Up.png"
#define Hero1down   "resources/heros/1hero_Down.png"
#define Hero1left   "resources/heros/1hero_Left.png"
#define Hero1right "resources/heros/1hero_Right.png"
#define Hero2up       "resources/heros/2hero_Up.png"
#define Hero2down   "resources/heros/2hero_Down.png"
#define Hero2left   "resources/heros/2hero_Left.png"
#define Hero2right "resources/heros/2hero_Right.png"
#define Hero3up       "resources/heros/3hero_Up.png"
#define Hero3down   "resources/heros/3hero_Down.png"
#define Hero3left   "resources/heros/3hero_Left.png"
#define Hero3right "resources/heros/3hero_Right.png"

#endif //COMMONDEF